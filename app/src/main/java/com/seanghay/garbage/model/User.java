package com.seanghay.garbage.model;

/**
 * Created by Seanghay on 6/2/2017.
 */

public class User {
    public enum UserType{DUSTMAN,USER}
    private int id;
    private String name;
    private String email;
    private UserType type;
    private String profileUrl;
    private int point;


    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public User() {
    }

    public User(int id, String name, String email, UserType type, String profileUrl) {

        this.id = id;
        this.name = name;
        this.email = email;
        this.type = type;
        this.profileUrl = profileUrl;
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserType getType() {
        return type;
    }

    public void setType(UserType type) {
        this.type = type;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }
}

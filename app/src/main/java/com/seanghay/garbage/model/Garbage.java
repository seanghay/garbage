package com.seanghay.garbage.model;

import android.graphics.Bitmap;
import android.net.Uri;

import java.util.List;

/**
 * Created by Seanghay on 6/2/2017.
 */

public class Garbage {
    private int id;
    private String userId;
    private String title;
    private String timestamp;
    private String description;
    private Location location;
    private boolean cleared;
    private Bitmap image;
    private Uri imageUrl;
    private List<String> notifiedList;
    
    public Uri getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(Uri imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public Garbage() {
    }

    public Garbage(int id, String userId, String title, String timestamp, String description, Location location, boolean cleared) {

        this.id = id;
        this.userId = userId;
        this.title = title;
        this.timestamp = timestamp;
        this.description = description;
        this.location = location;
        this.cleared = cleared;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public boolean isCleared() {
        return cleared;
    }

    public void setCleared(boolean cleared) {
        this.cleared = cleared;
    }
}

package com.seanghay.garbage.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.seanghay.garbage.R;
import com.seanghay.garbage.adapter.SocialPagerAdapter;

/**
 * Created by Seanghay on 6/2/2017.
 */

public class FragmentSocial extends Fragment {
    private TabLayout tabLayout;
    private ViewPager pager;
    private SocialPagerAdapter pagerAdapter;


    private AccountEditInterface editInterface;
    public interface AccountEditInterface{
        void onAccountView(int position);
    }

    public void setOnAccountView(AccountEditInterface editInterface){
        this.editInterface = editInterface;
    }




    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_social,container,false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
        setupTabs();

    }


    private void setupTabs() {
        tabLayout.addTab(tabLayout.newTab().setText("Garbage"));
        tabLayout.addTab(tabLayout.newTab().setText("People"));
        tabLayout.addTab(tabLayout.newTab().setText("Account"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        pagerAdapter = new SocialPagerAdapter(getFragmentManager(),tabLayout.getTabCount());
        pager.setAdapter(pagerAdapter);
        pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                pager.setCurrentItem(tab.getPosition());
                editInterface.onAccountView(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }



    public ViewPager getPager() {
        return pager;
    }

    public SocialPagerAdapter getPagerAdapter() {
        return pagerAdapter;
    }

    private void init(View view) {
        tabLayout = (TabLayout)view.findViewById(R.id.tab_layout);
        pager = (ViewPager)view.findViewById(R.id.pager);
    }
}

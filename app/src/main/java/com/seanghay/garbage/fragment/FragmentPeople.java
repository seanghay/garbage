package com.seanghay.garbage.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.seanghay.garbage.R;
import com.seanghay.garbage.adapter.PeopleAdapter;
import com.seanghay.garbage.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Seanghay on 6/2/2017.
 */

public class FragmentPeople extends Fragment{

    private List<User> userList;
    private PeopleAdapter adapter;
    private RecyclerView recyclerView;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userList = new ArrayList<>();

        User user = new User();
        user.setEmail("yathseanghay@gmail.com");
        user.setId(1234);
        user.setName("Yath Seanghay");
        user.setPoint(131);
        user.setType(User.UserType.USER);
        user.setProfileUrl("https://unsplash.it/120/120");

        userList.add(user);

        adapter = new PeopleAdapter(getContext(),userList);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_people,container,false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
    }
}

package com.seanghay.garbage.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.seanghay.garbage.R;
import com.seanghay.garbage.adapter.GarbageAdapter;
import com.seanghay.garbage.model.Garbage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Seanghay on 6/2/2017.
 */

public class FragmentGarbage extends Fragment {

    private RecyclerView recyclerView;
    private GarbageAdapter adapter;
    private List<Garbage> garbageList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_garbage,container,false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);

    }

    private void init(View view) {
        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        adapter = new GarbageAdapter(getContext(),garbageList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
    }

    public RecyclerView getRecyclerView() {
        return recyclerView;
    }

    public void setRecyclerView(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
    }

    public GarbageAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(GarbageAdapter adapter) {
        this.adapter = adapter;
    }

    public List<Garbage> getGarbageList() {
        return garbageList;
    }

    public void setGarbageList(List<Garbage> garbageList) {
        this.garbageList = garbageList;
    }
}

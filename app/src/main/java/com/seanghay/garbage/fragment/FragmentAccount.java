package com.seanghay.garbage.fragment;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.seanghay.garbage.R;
import com.seanghay.garbage.utils.GTextUtils;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Seanghay on 6/2/2017.
 */

public class FragmentAccount extends Fragment implements View.OnClickListener {

    private TextView tvUsername,tvEmail,tvPoint;
    private CircleImageView imageProfile;
    private FirebaseUser currentUser;
    private FirebaseAuth mAuth;
    private int userPoint = 78757;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account,container,false);
        return  view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
        initListener(view);
        initUser();
        onCompleted(view);
        startAnimation();
    }

    private void startAnimation() {
        ScaleAnimation expand =  new ScaleAnimation(0.2f, 1f, 0.2f, 1f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        expand.setDuration(150);
        expand.setInterpolator(new AccelerateInterpolator());
        imageProfile.startAnimation(expand);
    }

    private void onCompleted(final View view) {
        Picasso.with(getContext()).load("https://unsplash.it/1080/1520?blur").into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                view.setBackground(new BitmapDrawable(getContext().getResources(), bitmap));
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });
    }

    private void initUser() {
        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();
        Picasso.with(getContext())
                .load(currentUser.getPhotoUrl())
                .placeholder(R.mipmap.placeholder)
                .error(R.mipmap.placeholder)
                .fit()
                .into(imageProfile);
        tvUsername.setText(currentUser.getDisplayName());
        tvEmail.setText(currentUser.getEmail());
        tvPoint.setText(GTextUtils.numberFormat(userPoint) + " Points");
    }

    private void initListener(View view) {

    }


    private void init(View view) {
        tvUsername = (TextView)view.findViewById(R.id.user_name);
        tvEmail = (TextView)view.findViewById(R.id.user_email);
        tvPoint = (TextView)view.findViewById(R.id.user_point);
        imageProfile = (CircleImageView)view.findViewById(R.id.user_profile);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

        }
    }
}

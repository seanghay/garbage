package com.seanghay.garbage.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.seanghay.garbage.fragment.FragmentAccount;
import com.seanghay.garbage.fragment.FragmentGarbage;
import com.seanghay.garbage.fragment.FragmentPeople;

/**
 * Created by Seanghay on 6/2/2017.
 */

public class SocialPagerAdapter extends FragmentStatePagerAdapter {
    private int itemCount;

    private FragmentAccount fragmentAccount = new FragmentAccount();
    private FragmentPeople fragmentPeople = new FragmentPeople();
    private FragmentGarbage fragmentGarbage = new FragmentGarbage();

    public SocialPagerAdapter(FragmentManager fm, int itemCount) {
        super(fm);
        this.itemCount = itemCount;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return fragmentGarbage;
            case 1:
                return fragmentPeople;
            case 2:
                return fragmentAccount;
            default:
                return null;
        }
    }

    public int getItemCount() {
        return itemCount;
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }

    public FragmentAccount getFragmentAccount() {
        return fragmentAccount;
    }

    public void setFragmentAccount(FragmentAccount fragmentAccount) {
        this.fragmentAccount = fragmentAccount;
    }

    public FragmentPeople getFragmentPeople() {
        return fragmentPeople;
    }

    public void setFragmentPeople(FragmentPeople fragmentPeople) {
        this.fragmentPeople = fragmentPeople;
    }

    public FragmentGarbage getFragmentGarbage() {
        return fragmentGarbage;
    }

    public void setFragmentGarbage(FragmentGarbage fragmentGarbage) {
        this.fragmentGarbage = fragmentGarbage;
    }

    @Override
    public int getCount() {
        return itemCount;
    }
}

package com.seanghay.garbage.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.seanghay.garbage.R;
import com.seanghay.garbage.model.Garbage;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Seanghay on 6/2/2017.
 */

public class GarbageAdapter extends RecyclerView.Adapter<GarbageAdapter.ViewHolder> {
    private List<Garbage> data;
    private Context context;
   public GarbageAdapter(Context context, List<Garbage> data){
       this.data = data;
       this.context = context;
   }

    @Override
    public GarbageAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.garbage_layout,parent,false);
        return new ViewHolder(view);
    }




    @Override
    public void onBindViewHolder(GarbageAdapter.ViewHolder holder, int position) {
        Garbage garbage = data.get(position);
        holder.getTvDescription().setText(garbage.getDescription());
        holder.getTvDetail().setText(garbage.getTimestamp());
        holder.getTvProfileName().setText(garbage.getUserId());
        //holder.getImage().setImageBitmap(garbage.getImage());
        Picasso.with(context).load(garbage.getImageUrl()).fit().into(holder.getImage());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tvProfileName,tvDetail,tvDescription;
        private ImageView image;
        private CircleImageView profileImage;
        private Button btnNotify,btnCleaned;

        public TextView getTvProfileName() {
            return tvProfileName;
        }

        public void setTvProfileName(TextView tvProfileName) {
            this.tvProfileName = tvProfileName;
        }

        public TextView getTvDetail() {
            return tvDetail;
        }

        public void setTvDetail(TextView tvDetail) {
            this.tvDetail = tvDetail;
        }

        public TextView getTvDescription() {
            return tvDescription;
        }

        public void setTvDescription(TextView tvDescription) {
            this.tvDescription = tvDescription;
        }

        public ImageView getImage() {
            return image;
        }

        public void setImage(ImageView image) {
            this.image = image;
        }

        public CircleImageView getProfileImage() {
            return profileImage;
        }

        public void setProfileImage(CircleImageView profileImage) {
            this.profileImage = profileImage;
        }

        public Button getBtnNotify() {
            return btnNotify;
        }

        public void setBtnNotify(Button btnNotify) {
            this.btnNotify = btnNotify;
        }

        public Button getBtnCleaned() {
            return btnCleaned;
        }

        public void setBtnCleaned(Button btnCleaned) {
            this.btnCleaned = btnCleaned;
        }

        public ViewHolder(View itemView) {
            super(itemView);
            init(itemView);
        }

        private void init(View itemView) {
            tvProfileName = (TextView)itemView.findViewById(R.id.tvProfileName);
            tvDetail = (TextView)itemView.findViewById(R.id.profileDetails);
            tvDescription = (TextView)itemView.findViewById(R.id.tvDescription);
            image = (ImageView) itemView.findViewById(R.id.imageView);
            btnNotify = (Button) itemView.findViewById(R.id.button_notify);
            btnCleaned = (Button) itemView.findViewById(R.id.button_cleaned);

            btnNotify.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.button_notify:
                    if(btnNotify.getText().equals("Notify")){
                        btnNotify.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_notifications_black_24dp,0,0,0);
                        btnNotify.setText("Notified");
                    }else{
                        btnNotify.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_notifications_none_black_24dp,0,0,0);
                        btnNotify.setText("Notify");
                    }

                    break;
            }
        }
    }

}

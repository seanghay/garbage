package com.seanghay.garbage.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.seanghay.garbage.R;
import com.seanghay.garbage.model.User;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Seanghay on 6/3/2017.
 */

public class PeopleAdapter extends RecyclerView.Adapter<PeopleAdapter.ViewHolder> {
    private List<User> users;
    private Context context;

    public PeopleAdapter(Context context, List<User> users){
        this.context = context;
        this.users = users;
    }

    @Override
    public PeopleAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater  inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.people_layout,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PeopleAdapter.ViewHolder holder, int position) {
        User user = users.get(position);
        holder.getUsername().setText(user.getName());
        holder.getUserRole().setText("ROLE");
        holder.getUserPoint().setText(Integer.toString(user.getPoint()) + " Points");
        Picasso.with(context).load(user.getProfileUrl()).fit().into(holder.getProfile());
    }

    @Override
    public int getItemCount() {
        return users.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        private CircleImageView profile;
        private TextView username, userRole, userPoint;
        public ViewHolder(View itemView) {
            super(itemView);
            profile = (CircleImageView)itemView.findViewById(R.id.user_profile);
            userRole = (TextView)itemView.findViewById(R.id.role);
            username = (TextView)itemView.findViewById(R.id.user_name);
            userPoint = (TextView)itemView.findViewById(R.id.user_point);
        }


        public CircleImageView getProfile() {
            return profile;
        }

        public TextView getUsername() {
            return username;
        }

        public TextView getUserRole() {
            return userRole;
        }

        public TextView getUserPoint() {
            return userPoint;
        }
    }
}

package com.seanghay.garbage.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.seanghay.garbage.R;
import com.seanghay.garbage.service.GPSTracker;
import com.seanghay.garbage.utils.ConstHelper;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class PrePublishActivity extends AppCompatActivity implements View.OnClickListener {
    private Toolbar toolbar;
    private EditText tvDescription, tvLocation;
    private ImageView imageDust;
    private ImageButton btnLocation;
    private GPSTracker tracker;
    private LocationManager locationManager;
    private LocationListener locationListener;
    private String location;
    private com.seanghay.garbage.model.Location mLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_publish);
        init();
        prepareIntent();
        getLocation();
    }

    private void getLocation() {
        tracker = new GPSTracker(this);
        if(tracker.canGetLocation()){
            tvLocation.setText(getAddress(tracker.getLatitude(),tracker.getLongitude()));
            mLocation = new com.seanghay.garbage.model.Location();
            mLocation.setLatitude(tracker.getLatitude());
            mLocation.setLongitude(tracker.getLongitude());
        }

    }


    private void initService() {
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                tvLocation.setText("Lat : " + location.getLatitude() + " Long : "+location.getLongitude());
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        };
        getUserLocation();

    }

    private void getUserLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.INTERNET
                }, 10);
            }
            return;
        }
        locationManager.requestLocationUpdates("gps", 5000, 0, locationListener);
    }


    private void prepareIntent() {
        Intent intent = getIntent();
        if(intent.getExtras()!=null){
            String description = intent.getStringExtra(ConstHelper.EXTRA_DESCRIPTION);
            location = intent.getStringExtra(ConstHelper.EXTRA_LOCATION);
            Uri imageUrl = intent.getParcelableExtra(ConstHelper.EXTRA_IMAGE);
            tvLocation.setText(location);
            tvDescription.setText(description);
            Picasso.with(this).load(imageUrl).fit().into(imageDust);
        }
    }

    private void init() {
        btnLocation = (ImageButton)findViewById(R.id.button_location);
        imageDust = (ImageView)findViewById(R.id.imageDust);
        toolbar = (Toolbar) findViewById(R.id.pretoolbar);
        tvDescription = (EditText) findViewById(R.id.tv_description);
        tvLocation = (EditText)findViewById(R.id.tv_location);
        toolbar.setNavigationIcon(R.drawable.ic_close_black_24dp);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        btnLocation.setOnClickListener(this);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.publish_menu:
                publishClicked();
                return true;
        }
        return true;
    }

    private void publishClicked() {
        Intent intent = new Intent();
        intent.putExtra(ConstHelper.EXTRA_DESCRIPTION,tvDescription.getText().toString());
        intent.putExtra(ConstHelper.EXTRA_LOCATION,mLocation);
        setResult(RESULT_OK,intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.pre_menu,menu);
        return true;
    }

    public String getAddress(double lat, double lng) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            Address obj = addresses.get(0);
            String add = obj.getAddressLine(0);
            add = add + "\n" + obj.getAddressLine(1);
            add = add + "\n" + obj.getCountryName();
            return add;
        } catch (IOException e) {
            return String.valueOf(lat);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_location:

                break;
        }
    }


}

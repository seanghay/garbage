package com.seanghay.garbage.activity;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.marcoscg.easylicensesdialog.EasyLicensesDialogCompat;
import com.seanghay.garbage.R;
import com.seanghay.garbage.fragment.FragmentLearn;
import com.seanghay.garbage.fragment.FragmentMaps;
import com.seanghay.garbage.fragment.FragmentSocial;
import com.seanghay.garbage.model.Garbage;
import com.seanghay.garbage.model.Location;
import com.seanghay.garbage.service.GPSTracker;
import com.seanghay.garbage.utils.ConstHelper;
import com.seanghay.garbage.utils.ImageUtils;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.DateFormat;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.seanghay.garbage.R.id.fab;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, FragmentSocial.AccountEditInterface {

    private Toolbar toolbar;
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;
    private FragmentSocial fragmentSocial;
    private FragmentMaps fragmentMaps;
    private FragmentLearn fragmentLearn;
    private FloatingActionButton actionButton;
    private int currentDrawerMenu = R.id.menu_social;
    private Uri imageToUploadUri;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private int tabPosition = 0;
    private TextView tvSocialNotification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        initFirebase();
        initListener();
        setMenuCounter(R.id.menu_social,"99+");
    }

    private void setMenuCounter(@IdRes int itemId, String text) {
        tvSocialNotification = (TextView) navigationView.getMenu().findItem(itemId).getActionView();
        tvSocialNotification.setTextColor(ContextCompat.getColor(this,R.color.colorPrimary));
        tvSocialNotification.setText(text.isEmpty() ? null : text);
    }

    private void initFirebase() {
        mAuth = FirebaseAuth.getInstance();
    }

    private void initListener() {
        actionButton.setOnClickListener(this);
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    addNavHeader(user);
                } else {
                    Intent signInIntent = new Intent(MainActivity.this,SignInActivity.class);
                    startActivity(signInIntent);
                    finish();
                }
            }
        };
        fragmentSocial.setOnAccountView(this);
    }

    private void addNavHeader(FirebaseUser user) {
        View headerLayout = navigationView.getHeaderView(0);
        CircleImageView profile = (CircleImageView)headerLayout.findViewById(R.id.user_profile);
        TextView tvUserName = (TextView)headerLayout.findViewById(R.id.user_name);
        TextView tvEmail = (TextView)headerLayout.findViewById(R.id.user_email);
        Picasso.with(this)
                .load(user.getPhotoUrl())
                .placeholder(R.mipmap.placeholder)
                .error(R.mipmap.placeholder)
                .fit()
                .into(profile);

        tvEmail.setText(user.getEmail());
        tvUserName.setText(user.getDisplayName());
        profile.setOnClickListener(this);
        tvEmail.setOnClickListener(this);
        tvUserName.setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(mAuthListener!=null){
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    private void init() {
        fragmentSocial = new FragmentSocial();
        fragmentLearn = new FragmentLearn();
        fragmentMaps = new FragmentMaps();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        actionButton = (FloatingActionButton) findViewById(fab);
        navigationView = (NavigationView) findViewById(R.id.navigation_view);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        setSupportActionBar(toolbar);
        setupNavigationDrawer();
        menuSocialSelected();
        navigationView.setCheckedItem(R.id.menu_social);

    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawers();
            return;
        }
        super.onBackPressed();
    }


    private void setupNavigationDrawer() {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                currentDrawerMenu = item.getItemId();
                switch (item.getItemId()) {
                    case R.id.menu_social:
                        actionButton.show();
                        menuSocialSelected();
                        break;
                    case R.id.menu_maps:
                        menuMapSelected();
                        break;
                    case R.id.menu_learn:
                        actionButton.hide();
                        menuLearnSelected();
                        break;
                    case R.id.menu_feedback:
                        feedbackClicked();
                        drawerLayout.closeDrawers();
                        return true;
                    case R.id.menu_license:
                        openSourceLicenseClicked();
                        drawerLayout.closeDrawers();
                        return true;
                    case R.id.menu_exit:
                        exitClicked();
                        drawerLayout.closeDrawers();
                        return true;
                    case R.id.menu_sign_out:
                        signOutClicked();
                        drawerLayout.closeDrawers();
                        return true;
                    default:
                }

                drawerLayout.closeDrawers();
                item.setChecked(true);
                return true;
            }
        });

        toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open_drawer, R.string.close_drawer) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
    }

    private void openSourceLicenseClicked() {
        new EasyLicensesDialogCompat(this)
                .setTitle("Licenses")
                .setPositiveButton(android.R.string.ok, null)
                .show();
    }

    private void replaceFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.layout_container, fragment).commit();
    }

    private void signOutClicked() {
        AlertDialog.Builder signOutAlert = new AlertDialog.Builder(this)
                .setMessage("Are you sure you need to logout?")
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mAuth.signOut();
                    }
                });
        signOutAlert.show();
    }

    private void exitClicked() {
        finish();
    }

    private void feedbackClicked() {
        startFeedbackIntent();
    }


    private void startFeedbackIntent() {
        String phoneSpecs = "\n\n\n\n\n---------------------\nPHONE SPECS\n--------------------------\n";
        phoneSpecs += ( "SERIAL: " + Build.SERIAL + "\n" );
        phoneSpecs += ("MODEL: " + Build.MODEL + "\n" );
        phoneSpecs += ("ID: " + Build.ID + "\n" );
        phoneSpecs += ("Manufacture: " + Build.MANUFACTURER + "\n" );
        phoneSpecs += ("brand: " + Build.BRAND + "\n" );
        phoneSpecs += ("type: " + Build.TYPE + "\n" );
        phoneSpecs += ("user: " + Build.USER + "\n" );
        phoneSpecs += ("BASE: " + Build.VERSION_CODES.BASE + "\n" );
        phoneSpecs += ("INCREMENTAL " + Build.VERSION.INCREMENTAL + "\n" );
        phoneSpecs += ("SDK  " + Build.VERSION.SDK + "\n" );
        phoneSpecs += ("BOARD: " + Build.BOARD + "\n" );
        phoneSpecs += ("BRAND " + Build.BRAND + "\n" );
        phoneSpecs += ("HOST " + Build.HOST + "\n" );
        phoneSpecs += ("FINGERPRINT: "+Build.FINGERPRINT + "\n" );
        phoneSpecs += ("Version Code: " + Build.VERSION.RELEASE + "\n" );
        Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
        String aEmailList[] = {"seanghaydev@gmail.com"};
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, aEmailList);
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "[FEEDBACK] Garbage");
        emailIntent.setType("plain/text");
        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT,phoneSpecs);
        startActivity(emailIntent);
    }

    private void menuLearnSelected() {
        replaceFragment(fragmentLearn);
        setTitle(getString(R.string.learn));
    }

    private void menuMapSelected() {
        setTitle(getString(R.string.garbage_maps));
        replaceFragment(fragmentMaps);
        actionButton.show();
        actionButton.setImageResource(R.drawable.ic_my_location_black_24dp);
        actionButton.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.colorPrimaryDark));
    }

    private void menuSocialSelected() {
        replaceFragment(fragmentSocial);
        setTitle(getString(R.string.garbage));
        actionButton.setImageResource(R.drawable.ic_camera_alt_black_24dp);
        actionButton.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.colorAccent));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                fabClicked();
                break;
            case R.id.user_profile:
            case R.id.user_email:
            case R.id.user_name:
                if(currentDrawerMenu==R.id.menu_social)
                headerProfileClicked();
                break;
        }
    }

    private void headerProfileClicked() {
        drawerLayout.closeDrawers();
        fragmentSocial.getPager().setCurrentItem(2);
    }


    private void fabClicked() {
        switch (currentDrawerMenu) {
            case R.id.menu_social:
                switch (tabPosition) {
                    case 0:
                        postGarbage();
                        break;
                    case 1:
                        postGarbage();
                        break;
                    case 2:
                        onUserEdit();
                        break;
                }
                break;
            case R.id.menu_maps:
                getCurrentPositionClicked();
                break;
        }
    }

    private void onUserEdit() {
        Intent intentEditUser = new Intent(this,EditAccountActivity.class);
        startActivity(intentEditUser);
    }

    private void getCurrentPositionClicked() {
        GPSTracker tracker = new GPSTracker(this);
        if (tracker.canGetLocation()) {
            GoogleMap map = fragmentMaps.getMap();
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                map.setMyLocationEnabled(true);
                map.getUiSettings().setMyLocationButtonEnabled(false);
                LatLng loc = new LatLng (tracker.getLatitude(), tracker.getLongitude());
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 16.0f));
            } else {

            }
        }
    }


    private void askForPermission(String permission, Integer requestCode) {
        if (ContextCompat.checkSelfPermission(MainActivity.this, permission) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, permission)) {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{permission}, requestCode);
            } else {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{permission}, requestCode);
            }
        } else {
            openCamera();
        }
    }

    private void postGarbage() {
        startCamera();
    }

    private void startCamera() {
        askForPermission(Manifest.permission.CAMERA,ConstHelper.PERMISSION_CAMERA_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case ConstHelper.PERMISSION_CAMERA_REQUEST_CODE: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openCamera();
                } else {
                    Toast.makeText(this, "Permission denied!", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    private void openCamera() {
        Intent chooserIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = new File(Environment.getExternalStorageDirectory(), "POST_IMAGE " + System.currentTimeMillis() +".jpg");
        chooserIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
        imageToUploadUri = Uri.fromFile(file);
        startActivityForResult(chooserIntent, ConstHelper.CAMERA_REQUEST);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK){
            switch (requestCode){
                case ConstHelper.CAMERA_REQUEST:
                    if (requestCode == ConstHelper.CAMERA_REQUEST) {
                        if(imageToUploadUri != null){
                            Uri selectedImage = imageToUploadUri;
                            getContentResolver().notifyChange(selectedImage, null);
                            Bitmap reducedSizeBitmap = (new ImageUtils()).getBitmap(this,imageToUploadUri.getPath());
                            if(reducedSizeBitmap != null){
                                proceedImage();
                            }else{
                                Toast.makeText(this,"Error while capturing Image",Toast.LENGTH_LONG).show();
                            }
                        }else{
                            Toast.makeText(this,"Error while capturing Image",Toast.LENGTH_LONG).show();
                        }
                    }
                    break;
                case ConstHelper.PREVIEW_REQUEST:
                    String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
                    Garbage garbage = new Garbage();
                    garbage.setId(100);
                    garbage.setDescription(data.getStringExtra(ConstHelper.EXTRA_DESCRIPTION));
                    garbage.setTimestamp(currentDateTimeString);
                    garbage.setLocation((Location) data.getExtras().get(ConstHelper.EXTRA_LOCATION));
                    garbage.setUserId(mAuth.getCurrentUser().getUid().toString());
                    garbage.setImageUrl(imageToUploadUri);

                    fragmentSocial.getPagerAdapter().getFragmentGarbage().getGarbageList().add(garbage);
                    fragmentSocial.getPagerAdapter().getFragmentGarbage().getAdapter().notifyDataSetChanged();
                    break;
            }
        }
    }

    private void proceedImage() {
        Intent previewIntent = new Intent(getBaseContext(),PrePublishActivity.class);
        previewIntent.putExtra(ConstHelper.EXTRA_DESCRIPTION,"description");
        previewIntent.putExtra(ConstHelper.EXTRA_LOCATION,"location");
        previewIntent.putExtra(ConstHelper.EXTRA_IMAGE,imageToUploadUri);
        startActivityForResult(previewIntent,ConstHelper.PREVIEW_REQUEST);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu_search,menu);
        return true;
    }

    @Override
    public void onAccountView(int position) {
        if(position==2 || tabPosition == 2) animateFab(position);
        tabPosition = position;
    }

    int[] colorIntArray = {R.color.colorAccent,R.color.colorAccent,R.color.gold_material};
    int[] iconIntArray = {R.drawable.ic_camera_alt_black_24dp,R.drawable.ic_camera_alt_black_24dp,R.drawable.ic_edit_black_24dp};

    protected void animateFab(final int position) {
        actionButton.clearAnimation();
        ScaleAnimation shrink =  new ScaleAnimation(1f, 0.2f, 1f, 0.2f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        shrink.setDuration(70);
        shrink.setInterpolator(new DecelerateInterpolator());
        shrink.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                actionButton.setImageResource(iconIntArray[position]);
                actionButton.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), colorIntArray[position]));
                ScaleAnimation expand =  new ScaleAnimation(0.2f, 1f, 0.2f, 1f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                expand.setDuration(70);     // animation duration in milliseconds
                expand.setInterpolator(new AccelerateInterpolator());
                actionButton.startAnimation(expand);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        actionButton.startAnimation(shrink);
    }

}

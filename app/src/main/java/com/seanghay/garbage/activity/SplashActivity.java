package com.seanghay.garbage.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;

import com.seanghay.garbage.R;
import com.seanghay.garbage.utils.ConstHelper;

public class SplashActivity extends Activity {
    private Class nextActivity = SignInActivity.class;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        checkCondition();
    }

    private void checkCondition() {
        SharedPreferences preferences = getPreferences(Context.MODE_PRIVATE);
        boolean isShown = preferences.getBoolean(getString(R.string.first_start_splash),false);
        if(!isShown){
            init();
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean(getString(R.string.first_start_splash),true);
            editor.commit();
        }else{
            startNextActivity();
        }
    }

    private void init() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startNextActivity();
            }
        }, ConstHelper.SPLASH_TIME_OUT);
    }

    private void startNextActivity() {
        Intent nextIntent = new Intent(SplashActivity.this, nextActivity);
        startActivity(nextIntent);
        finish();
    }
}

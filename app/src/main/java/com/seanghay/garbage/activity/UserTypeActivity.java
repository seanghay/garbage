package com.seanghay.garbage.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.seanghay.garbage.R;

public class UserTypeActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btnNext;
    private Class nextActivity = SignInActivity.class;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_type);
        init();
    }

    private void init() {
        btnNext = (Button)findViewById(R.id.button_next);
        btnNext.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_next:
                nextActivity();
                break;
        }
    }

    private void nextActivity() {
        Intent registerIntent = new Intent(this,nextActivity);
        startActivity(registerIntent);
    }
}

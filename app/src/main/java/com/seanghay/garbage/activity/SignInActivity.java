package com.seanghay.garbage.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.seanghay.garbage.R;
import com.seanghay.garbage.utils.ConstHelper;
import com.seanghay.garbage.utils.GTextUtils;

public class SignInActivity extends AppCompatActivity implements View.OnClickListener {
    private Toolbar toolbar;
    private Button btnSignIn;
    private CheckBox cbRemember;
    private EditText etEmail,etPassword;
    private TextView tvCreateAccount;
    private ProgressBar progress;
    private UserProfileChangeRequest profileUpdates;
    private Class registerClass = RegisterActivity.class,
            mainActivityClass = MainActivity.class;
    private boolean isRegister = false;

    private int i=1;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        mAuth = FirebaseAuth.getInstance();
        init();
        initListener();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(mAuthListener!=null){
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }


    private void initListener() {
        btnSignIn.setOnClickListener(this);
        tvCreateAccount.setOnClickListener(this);
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if(user!=null) {
                    startMainActivity();
                }
            }
        };
    }

    private void showMessage(String message){
        Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), message, Snackbar.LENGTH_SHORT);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(ContextCompat.getColor(this,R.color.colorAccent));

        TextView textView = (TextView)snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_info, 0, 0, 0);
        textView.setCompoundDrawablePadding(23);
        snackbar.show();
    }

    private void init() {
        toolbar = (Toolbar)findViewById(R.id.login_toolbar);
        setSupportActionBar(toolbar);
        progress = (ProgressBar)findViewById(R.id.progress_sign_in);
        tvCreateAccount = (TextView)findViewById(R.id.create_account);
        btnSignIn = (Button)findViewById(R.id.button_sign_in);
        cbRemember = (CheckBox)findViewById(R.id.checkbox_remember);
        etEmail = (EditText)findViewById(R.id.et_email);
        etPassword = (EditText)findViewById(R.id.et_password);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_sign_in:
                signInClicked();
                break;
            case R.id.create_account:
                signUpClicked();
                break;
        }
    }

    private void signUpClicked() {
        Intent signUpIntent = new Intent(getBaseContext(),registerClass);
        startActivityForResult(signUpIntent, ConstHelper.RESULT_REGISTER);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK){
            switch (requestCode){
                case ConstHelper.RESULT_REGISTER:
                    if(data!=null){

                    }
                    break;
            }
        }
    }



    private void signInClicked() {
        String email,password;
        email = etEmail.getText().toString().trim();
        password = etPassword.getText().toString().trim();
        if(!(email.isEmpty() || password.isEmpty())) {
            if(GTextUtils.isValidEmaillId(email)) {
                btnSignIn.setVisibility(View.GONE);
                progress.setVisibility(View.VISIBLE);
                mAuth.signInWithEmailAndPassword(etEmail.getText().toString(), etPassword.getText().toString())
                        .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (!task.isSuccessful()) {
                                    etEmail.requestFocus();
                                    etPassword.getText().clear();
                                    showMessage(getString(R.string.login_failed));
                                    btnSignIn.setVisibility(View.VISIBLE);
                                    progress.setVisibility(View.GONE);
                                }
                            }
                        });
            }else{
                showMessage(getString(R.string.invalid_email));
                etEmail.requestFocus();
            }
        }else {
            showMessage(getString(R.string.empty_edit_text));
            etEmail.requestFocus();
        }
    }

    private void startMainActivity() {
        Intent mainIntent = new Intent(getBaseContext(),mainActivityClass);
        startActivity(mainIntent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.register_menu,menu);
        return true;
    }


}

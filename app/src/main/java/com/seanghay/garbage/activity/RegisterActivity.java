package com.seanghay.garbage.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.seanghay.garbage.R;
import com.seanghay.garbage.utils.ConstHelper;
import com.seanghay.garbage.utils.GTextUtils;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import de.hdodenhof.circleimageview.CircleImageView;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private CircleImageView imageProfile;
    private Button btnRegister;
    private EditText etEmail,etUsername,etPassword;
    private CheckBox checkBoxTerm;
    private View loadingLayout;
    private CoordinatorLayout coordinatorLayout;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private StorageReference mStorageRef;
    private Uri imageProfileUri;
    private boolean customProfile = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initFirebase();
        init();
        initListener();
    }

    private void initFirebase() {
        mAuth = FirebaseAuth.getInstance();
        mStorageRef = FirebaseStorage.getInstance().getReference();

    }

    private void initListener() {
        btnRegister.setOnClickListener(this);
        imageProfile.setOnClickListener(this);
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                final FirebaseUser user = firebaseAuth.getCurrentUser();
                if(user!=null){
                    StorageReference profileRef = mStorageRef.child(ConstHelper.PROFILE_PATH + etUsername.getText().toString().trim() + ".jpg");
                    profileRef.putFile(imageProfileUri)
                            .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    Uri downloadUrl = taskSnapshot.getDownloadUrl();
                                    UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                            .setDisplayName(etUsername.getText().toString().trim())
                                            .setPhotoUri(downloadUrl).build();
                                    user.updateProfile(profileUpdates);
                                    Intent intent  = new Intent();
                                    intent.putExtra(ConstHelper.__EXTRA_EMAIL,user.getEmail());
                                    intent.putExtra(ConstHelper.__EXTRA_USERNAME,user.getDisplayName());
                                    intent.putExtra(ConstHelper.__EXTRA_PASSWORD,etPassword.getText().toString().trim());
                                    intent.putExtra(ConstHelper.__EXTRA_PROFILE_URL,downloadUrl.toString());
                                    setResult(RESULT_OK,intent);
                                    finish();
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    showMessage(getString(R.string.register_failed));
                                    setLoading(false);
                                }
                            });
                }
            }
        };
    }


    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(mAuthListener!=null)
            mAuth.removeAuthStateListener(mAuthListener);
    }

    private void init() {
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        imageProfile = (CircleImageView)findViewById(R.id.image_profile);
        btnRegister = (Button)findViewById(R.id.button_register);
        etEmail = (EditText)findViewById(R.id.et_email);
        etPassword = (EditText)findViewById(R.id.et_password);
        etUsername = (EditText)findViewById(R.id.et_username);
        checkBoxTerm = (CheckBox)findViewById(R.id.checkbox_term);
        toolbar = (Toolbar)findViewById(R.id.register_toolbar);
        loadingLayout = findViewById(R.id.loading_layout);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.register_menu,menu);
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_register:
                onRegisterClicked();
                break;
            case R.id.image_profile:
                selectImageClicked();
                break;
        }
    }

    private void selectImageClicked() {
        CropImage.activity()
                .setAspectRatio(1,1)
                .setOutputCompressFormat(Bitmap.CompressFormat.JPEG)
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this);
    }

    private void onRegisterClicked() {
        String username, email,password;
        username = etUsername.getText().toString().trim();
        password = etPassword.getText().toString().trim();
        email = etEmail.getText().toString().trim();
        if(checkBoxTerm.isChecked()) {
            if (customProfile) {
                if (!(username.isEmpty() || email.isEmpty() || password.isEmpty())) {
                    if (GTextUtils.isValidEmaillId(email)) {

                        if(password.length()>8){
                            setLoading(true);
                            startRegistration(username, email, password);
                        }else{
                            showMessage("Password must be greater than 8 characters");
                        }
                    } else {
                        etEmail.requestFocus();
                        showMessage(getString(R.string.invalid_email));
                    }
                } else {
                    showMessage(getString(R.string.empty_edit_text));
                    etUsername.requestFocus();
                }
            } else {
                selectImageClicked();
                showMessage("Please choose your image profile");
            }
        }else{
            showMessage("Please accept terms & condition!");
            checkBoxTerm.requestFocus();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK){
            switch (requestCode){
                case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                    insertProfile(data);
                    break;

            }
        }else if(resultCode==CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE){
            showMessage("Cannot crop image");
            customProfile = false;
        }
    }



    private void showMessage(String message){
        Snackbar snackbar = Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_SHORT);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(ContextCompat.getColor(this,R.color.colorAccent));

        TextView textView = (TextView)snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_info, 0, 0, 0);
        textView.setCompoundDrawablePadding(23);
        snackbar.show();
    }


    private void insertProfile(Intent data) {
        CropImage.ActivityResult result = CropImage.getActivityResult(data);
        imageProfileUri = result.getUri();
        Picasso.with(this).load(imageProfileUri).fit().into(imageProfile);
        customProfile = true;
    }


    void setLoading(boolean loading){
        if(loading)loadingLayout.setVisibility(View.VISIBLE);
        else loadingLayout.setVisibility(View.GONE);
        etEmail.setEnabled(!loading);
        etPassword.setEnabled(!loading);
        etUsername.setEnabled(!loading);
        btnRegister.setEnabled(!loading);
        checkBoxTerm.setEnabled(!loading);
    }

    private void startRegistration(final String username, final String email, final String password) {
        mAuth.createUserWithEmailAndPassword(email,password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){


                        }else{
                            Toast.makeText(RegisterActivity.this, R.string.register_failed,Toast.LENGTH_LONG).show();
                            setLoading(false);
                        }
                    }
                });
    }
}

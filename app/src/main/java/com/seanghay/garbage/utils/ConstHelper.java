package com.seanghay.garbage.utils;

public class ConstHelper {
    public static final long SPLASH_TIME_OUT = 200 ;
    public static final int CAMERA_REQUEST = 132;
    public static final int PREVIEW_REQUEST = 427;
    public static final String EXTRA_DESCRIPTION = "__description";
    public static final String __EXTRA_PROFILE_URL = "__profile_URL";
    public static final String EXTRA_LOCATION = "__location";
    public static final String EXTRA_IMAGE = "__image";
    public static final int RESULT_REGISTER = 95;
    public static final String __EXTRA_USERNAME = "___extra_username",
                                __EXTRA_PASSWORD = "___extra_password",
                                __EXTRA_EMAIL = "__extra_email";
    public static final int RESULT_IMAGE_PICKER_CODE = 164;
    public static final int RESULT_IMAGE_CROP_CODE = 141;
    public static final int PERMISSION_CAMERA_REQUEST_CODE = 481,
                            PERMISSION_WRITE_EXTERNAL_STORAGE_CODE = 429;
    public static final String PROFILE_PATH = "profiles/";

}
